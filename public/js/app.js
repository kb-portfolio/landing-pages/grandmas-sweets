$(function() {
    document.getElementById("cart-info").addEventListener("click", function() {
        const cart = document.getElementById("cart");
        cart.classList.toggle("show-cart");
    });
    // navbar scroll
    function scrollToSection(ev) {
        ev.preventDefault();
        const $section = $($(this).attr('href'));
        $('html, body').animate({
            scrollTop: $section.offset().top - 75
        }, 500);
    }
    $('[data-scroll]').on('click', scrollToSection);

    //filter
    $('#store .sortBtn').on('click', 'a', function (ev) {
        ev.preventDefault();

        $('#store .sortBtn a.active').removeClass('active');

        $(ev.target).addClass('active');
        const currentCategory = $(this).data('filter');
        $.map($('#store-items .store-item'), function (item) {
            ;
            if ($(item).data('item') ===  currentCategory) {
                $(item).show();
            } else if (currentCategory === 'all') {
                $(item).show();
            } else {
                $(item).hide();
            }
        });
    });

    //search filter
    $('#store #search-item').on('keyup', function () {
        const value = $(this).val().toLowerCase();
        $('#store .store-item').filter(function () {
            $(this).toggle($(this).find('.store-item-name').text().toLowerCase().indexOf(value) > -1)
        });
    });

});